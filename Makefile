init : cabal.sandbox.config 
	cabal configure --enable-test
	cabal install --only-dependencies

build :
	cabal build

test :
	cabal test

cabal.sandbox.config :
	cabal sandbox init

.PHONY : build test
