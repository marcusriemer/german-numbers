{-# LANGUAGE ImplicitParams #-}

module Main where

import GermanNumbers

import Prelude hiding (getContents,putStrLn)
import System.IO.Encoding
import System.Environment

import Text.Read
import Network.CGI
import Data.Encoding.UTF8


-- Decides whether a string should be converted to a string
-- or to an integer.
decide :: String -> Either Integer String
decide arg = case (readMaybe arg :: Maybe Integer) of
  Nothing -> Right arg
  Just v  -> Left  v

-- Decides for each line how it should be treated
parseLines :: [String] -> [Either Integer String]
parseLines args =
  if length args == 0
    then []
    else map (singleArg args) [0..(length args) -1]
  where
    singleArg args n = decide $ args !! n

-- Retrieve the numbers from stdin
readStdIn :: IO([Either Integer String])
readStdIn = do
  let ?enc = UTF8
  stdin <- getContents
  let stdinByLine = lines stdin
  return $ parseLines stdinByLine
  

main :: IO()
main = do
  let ?enc = UTF8
  putStrLn "Content-type: text/plain; charset=utf-8"
  putStrLn ""
  
  stdin <- readStdIn
  mapM_ printNum (stdin)
  where
    printNum num =
      case num of
        Left  v -> putStrLn $ toGermanPhonetic v
        Right v -> fromGermanPhonetic' v
