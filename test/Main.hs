{-# OPTIONS_GHC -fno-warn-orphans #-}
module Test where

import Data.Monoid

import Test.Framework
import Test.Framework.Providers.HUnit
import Test.Framework.Providers.QuickCheck2
import Test.HUnit
import Test.QuickCheck.Test

import GermanNumbers
import FromString

-- We need to know about parser errors
import Text.ParserCombinators.Parsec.Error(ParseError, Message, errorMessages, messageEq)

instance Eq ParseError where
   a == b = errorMessages a == errorMessages b


caseToString :: Integer -> String -> Test.Framework.Test
caseToString num str = testCase (show num) $ str @=? (toGermanPhonetic num)

testToString = [ caseToString         1 "eins"
               , caseToString        12 "zwölf"                                
               , caseToString        13 "dreizehn"                             
               , caseToString        21 "einundzwanzig"                        
               , caseToString       100 "einhundert"                           
               , caseToString       101 "einhunderteins"                       
               , caseToString       130 "einhundertdreißig"                    
               , caseToString       132 "einhundertzweiunddreißig"             
               , caseToString      1000 "eintausend"
               , caseToString      1001 "eintausendeins"
               , caseToString      1002 "eintausendzwei"                       
               , caseToString      1010 "eintausendzehn"                       
               , caseToString      1011 "eintausendelf"                        
               , caseToString      1014 "eintausendvierzehn"                   
               , caseToString      5230 "fünftausendzweihundertdreißig"        
               , caseToString      9999 "neuntausendneunhundertneunundneunzig" 
               , caseToString   1000000 "eine Million"
               , caseToString   1010000 "eine Million zehntausend"
               , caseToString   1110000 "eine Million einhundertzehntausend"
               , caseToString   1100000 "eine Million einhunderttausend"
               , caseToString   1000478 "eine Million vierhundertachtundsiebzig"
               , caseToString   2000000 "zwei Millionen"                    
               ]

caseDecimal :: [(Integer, Integer)] -> Integer -> Test.Framework.Test
caseDecimal lst expected = testCase text (expected @=? (calculate decLst))
  where
    decLst = map dp lst
    text   = ((show lst) ++ " = " ++ (show expected))
    
testCalculateDecimal = [ caseDecimal [(1,1), (2,2)]             210 
                       , caseDecimal [(1,0)]                      1
                       , caseDecimal [(1,1)]                     10
                       , caseDecimal [(1,1), (1,0)]              11
                       , caseDecimal [(2,0)]                      2
                       , caseDecimal [(2,1)]                     20
                       , caseDecimal [(1,3)]                   1000
                       , caseDecimal [(1,0),(1,1),(1,2),(1,3)] 1111
                       ]

caseFromString :: Integer -> String -> Test.Framework.Test
caseFromString v t = testCase t $ (Right v) @=? (fromGermanPhonetic t)

testFromString = [ caseFromString 1    "eins"
                 , caseFromString 6    "sechs"
                 , caseFromString 7    "sieben"
                 , caseFromString 10   "zehn"
                 , caseFromString 11   "elf"
                 , caseFromString 12   "zwölf"
                 , caseFromString 13   "dreizehn"
                 , caseFromString 21   "einundzwanzig"
                 , caseFromString 34   "vierunddreißig"
                 , caseFromString 99   "neunundneunzig"
                 , caseFromString 200  "zweihundert"
                 , caseFromString 208  "zweihundertacht"
                 , caseFromString 208  "zweihundertundacht"
                 , caseFromString 212  "zweihundertzwölf"
                 , caseFromString 1000 "eintausend"
                 , caseFromString 1700 "eintausendsiebenhundert"
                 , caseFromString 3711 "dreitausendsiebenhundertelf"
                 , caseFromString 1011 "eintausendelf"
                 , caseFromString 9990 "neuntausendneunhundertneunzig"
                 ]

caseEquilibrium :: Integer -> Test.Framework.Test
caseEquilibrium n = testCase (show n) $ (Right n) @=? (fromGermanPhonetic $ toGermanPhonetic n)

testEquilibrium = map caseEquilibrium [1..99999] 

propFromStringToString n = (fromGermanPhonetic $ toGermanPhonetic n) == (Right n)

main = defaultMainWithOpts tests mempty
  where tests = [ testGroup "toString" testToString
                , testGroup "calculateDecimal" testCalculateDecimal
                , testGroup "fromString" testFromString
                , testGroup "Equilibrium" testEquilibrium
                ]

