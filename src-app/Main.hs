module Main where

import GermanNumbers

import Text.Read
import System.Environment

-- Decides whether a string should be converted to a string
-- or to an integer.
decide :: String -> Either Integer String
decide arg = case (readMaybe arg :: Maybe Integer) of
  Nothing -> Right arg
  Just v  -> Left  v

-- Decides for each line how it should be treated
parseLines :: [String] -> [Either Integer String]
parseLines args =
  if length args == 0
    then []
    else map (singleArg args) [0..(length args) -1]
  where
    singleArg args n = decide $ args !! n


-- Retrieve the numbers from the commandline
readArgs :: IO([Either Integer String])
readArgs = do
  args <- getArgs
  return $ parseLines args

-- Retrieve the numbers from stdin
readStdIn :: IO([Either Integer String])
readStdIn = do
  stdin <- getContents
  let stdinByLine = lines stdin
  return $ parseLines stdinByLine
  

main :: IO()
main = do
  args <- readArgs
  stdin <- readStdIn

  mapM_ printNum (args ++ stdin)
  where
    printNum num =
      case num of
        Left  v -> putStrLn $ toGermanPhonetic v
        Right v -> fromGermanPhonetic' v
