module ToString where

import Data.Char(isSpace)

type Place       = Maybe Integer
data DecimalType = Singular | Plural | None
  deriving (Eq, Show)

-- Ok, this isn't horribly efficient but should to the trick
-- for these small strings we are working with here
trim :: String -> String
trim = f . f
   where f = reverse . dropWhile isSpace

toGermanPhonetic :: Integer -> String
toGermanPhonetic n = 
  trim $ billions ++ millions ++ thousands ++ hundreds ++ tens
  where
    digitized    = digs n
    billions     = formatGreaterHundreds digitized 9 (decimalString " Milliarde " " Milliarden ") "eine"
    millions     = formatGreaterHundreds digitized 6 (decimalString " Million "   " Millionen ")  "eine"
    thousands    = formatGreaterHundreds digitized 3 (decimalString "tausend"     "tausend"    )  "ein"
    hundreds     = formatHundreds  (maybeIndex 2 digitized)
    tens         = formatTens      (maybeIndex 1 digitized) (maybeIndex 0 digitized) "eins"

-- Takes care of formatting the recurring patterns starting at thousands.
-- All of these use hundreds, tens and ones as recurring names
formatGreaterHundreds :: [Integer] -> Int -> (DecimalType -> String) -> String -> String
formatGreaterHundreds digitized i name one =
  hundreds ++ tens ++ nameString
  where
    nameString   = if hasNumbers then name (decimalType digitized i) else ""
    hundreds     = formatHundreds (maybeIndex (i + 2) digitized)
    tens         = formatTens     (maybeIndex (i + 1) digitized) (maybeIndex i digitized) one
    hasNumbers   = length digitized > i && any ( /= 0) (take 3 $ drop i digitized)

-- Determines the numerus of the german word that should be used
-- according for a given subset of the whole number
decimalType :: [Integer] -> Int -> DecimalType
decimalType digitized i
  | all (== 0) relevant  = None
  | onlyDigitOne         = Singular
  | otherwise            = Plural
  where
    relevant     = takeOrZero 3 $ drop i digitized
    onlyDigitOne = (all (== 0) $ drop 1 relevant) && (1 == head relevant)

decimalString :: String -> String -> DecimalType -> String
decimalString _        _      None     = ""
decimalString singular _      Singular = singular
decimalString _        plural Plural   = plural

-- Works similar to take, but puts a 0 into the list instead of returning
-- no item at all.
takeOrZero :: (Num a) => Int -> [a] -> [a]
takeOrZero 0 _      = []
takeOrZero i []     = 0 : takeOrZero (i - 1) [] 
takeOrZero i (x:xs) = x : takeOrZero (i - 1) xs

-- Converts an Integer to a list of its digits
digs :: Integral x => x -> [x]
digs 0 = []
digs x = x `mod` 10 : digs (x `div` 10)

-- Safely allows access to non existing list elements
maybeIndex i lst
 | i >= length lst = Nothing
 | otherwise       = Just $ lst !! i


formatTens :: Place -> Place -> String -> String
formatTens Nothing       Nothing     _   = ""
formatTens Nothing      (Just digit) one = germanDigit digit one
formatTens (Just tenth) (Just digit) one
  | numValue == 0  = ""
  | numValue <= 12 =  germanDigit numValue one 
  | numValue <= 19 = (germanDigit digit    one) ++ (germanTens 1)
  | tenth    <= 9  = (andShortGermanDigit digit) ++ (germanTens tenth)
  where
    numValue     = (tenth * 10 + digit)

formatHundreds :: Place -> String
formatHundreds Nothing = ""
formatHundreds (Just hundreth)
  | hundreth == 0 = ""
  | hundreth <= 9 = germanHundreds hundreth

formatThousands :: Bool -> String
formatThousands has 
  | (has == True)    = "tausend"
  | (has == False)   = ""

formatMillions :: Bool -> String
formatMillions has
  | has == True  = " Millionen "
  | has == False = ""

shortGermanDigit n = germanDigit n "ein"

andShortGermanDigit n
  | n == 0 = ""
  | n <= 9 = shortGermanDigit n ++ "und"

germanDigit n one
  | n == 0  = "null"
  | n == 1  = one
  | n == 2  = "zwei"
  | n == 3  = "drei"
  | n == 4  = "vier"
  | n == 5  = "fünf"
  | n == 6  = "sechs"
  | n == 7  = "sieben"
  | n == 8  = "acht"
  | n == 9  = "neun"
  | n == 10 = "zehn"
  | n == 11 = "elf"
  | n == 12 = "zwölf"

germanTens n
  | n == 1 = "zehn"
  | n == 2 = "zwanzig"
  | n == 3 = "dreißig"
  | n == 4 = "vierzig"
  | n == 5 = "fünfzig"
  | n == 6 = "sechzig"
  | n == 7 = "siebzig"
  | n == 8 = "achtzig"
  | n == 9 = "neunzig"

germanHundreds n
  | n <= 9 = (shortGermanDigit n) ++ "hundert"
