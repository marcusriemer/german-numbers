{-# LANGUAGE OverloadedStrings #-}

module FromString (fromGermanPhonetic, fromGermanPhonetic', calculate, DecimalPlace, dp) where

import Text.ParserCombinators.Parsec hiding (digit)

fromGermanPhonetic  :: String -> Either ParseError Integer
fromGermanPhonetic = parse parseImpl "Error"

fromGermanPhonetic' :: String -> IO()
fromGermanPhonetic' = parseTest parseImpl

parseImpl :: Parser Integer
parseImpl = parseTop >>= return . calculate

parseTop :: Parser [DecimalPlace]
parseTop = do
  billionth'  <- optionMaybe $ try parseBillionth
  millionth'  <- optionMaybe $ try parseMillionth
  thousandth' <- optionMaybe $ try parseThousandth
  hundreth'   <- optionMaybe $ try $ parseHundreth 2
  tenth'      <- optionMaybe $ try $ parseTenth 0
  eof
  return $ concatResults [billionth', millionth', thousandth', hundreth', tenth']

concatResults :: [Maybe [DecimalPlace]] -> [DecimalPlace]
concatResults []             = []
concatResults (Nothing : xs) = concatResults xs
concatResults ((Just v): xs) = v ++ concatResults xs


setDecimalPlace :: Integer -> DecimalPlace -> DecimalPlace
setDecimalPlace i (DecimalPlace digit _) = DecimalPlace digit i

-- Because the german language swaps the single digits
-- and the tenth digits place, this step actually parses the
-- last two digits.
parseTenth :: Integer -> Parser [DecimalPlace]
parseTenth place = try parseBoth <|> try parseSingle  <|> try (parseSingleWords place)
  where
    parseBoth = do
      digitValue <- digit
      optional $ string "und"
      tenthValue <- tenth (place + 1)
      return $ [DecimalPlace digitValue place, tenthValue]
    parseSingle = do
      tenthValue <- tenth (place + 1)
      return $ [tenthValue]

-- Used to parse special words that potentially have a meaning
-- involving two digit places
parseSingleWords :: Integer -> Parser [DecimalPlace]
parseSingleWords i =  (try $ digit >>= \val -> (return $ [DecimalPlace val i]))
                  <|> (string "elf"         >> (return $ [dp (1,i+1), dp (1,i)]))
                  <|> (string "zwölf"       >> (return $ [dp (1,i+1), dp (2,i)]))


parseHundreth :: Integer -> Parser [DecimalPlace]
parseHundreth place = do
  digitValue <- digit
  string "hundert"
  optional $ string "und"
  return $ [(DecimalPlace digitValue place)]

parseThousandth :: Parser [DecimalPlace]
parseThousandth = try plural <|> try singular
  where
    singular = do
      optional $ string "ein"
      string "tausend"
      return [(DecimalPlace 1 3)]
    plural = do
      hundreth   <- optionMaybe $ try $ parseHundreth 5
      tenth      <- optionMaybe $ try $ parseTenth    3
      string "tausend"
      optional $ string "und"
      return $ concatResults [hundreth,tenth]

parseMillionth :: Parser [DecimalPlace]
parseMillionth = try plural <|> try singular
  where
    singular = do
      optional $ string "eine "
      string "Million"
      optional spaces
      return [(DecimalPlace 1 6)]
    plural = do
      hundreth   <- optionMaybe $ try $ parseHundreth 8
      tenth      <- optionMaybe $ try $ parseTenth    6
      spaces
      string "Millionen"
      optional spaces
      return $ concatResults [hundreth, tenth]

parseBillionth :: Parser [DecimalPlace]
parseBillionth = try plural <|> try singular
  where
    singular = do
      optional $ string "eine "
      string "Milliarde"
      optional spaces
      return [(DecimalPlace 1 9)]
    plural = do
      hundreth   <- optionMaybe $ try $ parseHundreth 12
      tenth      <- optionMaybe $ try $ parseTenth    9
      spaces
      string "Milliarden"
      optional spaces
      return $ concatResults [hundreth, tenth]


-- Matches exactly one of the possible digits
digit :: Parser Integer
digit  =  (string "ein" >> (optional $ string "s") >> return 1)
      <|> (      string "zwei"                     >> return 2)
      <|> (      string "drei"                     >> return 3)
      <|> (      string "vier"                     >> return 4)
      <|> (      string "fünf"                     >> return 5)
      <|> (try $ string "sechs"                    >> return 6)
      <|> (      string "sieben"                   >> return 7)
      <|> (      string "acht"                     >> return 8)
      <|> (      string "neun"                     >> return 9)

tenth :: Integer -> Parser DecimalPlace
tenth place =  (try $ string "zehn"    >> (return $ DecimalPlace 1 place))
           <|> (try $ string "zwanzig" >> (return $ DecimalPlace 2 place))
           <|> (      string "dreißig" >> (return $ DecimalPlace 3 place))
           <|> (      string "vierzig" >> (return $ DecimalPlace 4 place))
           <|> (      string "fünfzig" >> (return $ DecimalPlace 5 place))
           <|> (try $ string "sechzig" >> (return $ DecimalPlace 6 place))
           <|> (try $ string "siebzig" >> (return $ DecimalPlace 7 place))
           <|> (      string "achtzig" >> (return $ DecimalPlace 8 place))
           <|> (      string "neunzig" >> (return $ DecimalPlace 9 place))


-- Defines a pair that has the digit n at a certain place
data DecimalPlace = DecimalPlace Integer Integer

dp      = \ (value, place) -> DecimalPlace value place

-- Present it the same way as tuples
instance Show DecimalPlace where
  show (DecimalPlace value place) = show (value,place)

calculate :: [DecimalPlace] -> Integer
calculate []                            = 0
calculate ((DecimalPlace n place) : xs) = (10 ^ place) * n + calculate xs

